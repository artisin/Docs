# Discovery, comet contains alcohol!

Comet Lovejoy lived up to its name by releasing large amounts of alcohol as well as a type of sugar into space, according to new observations by an international team. The discovery marks the first time ethyl alcohol, the same type in alcoholic beverages, has been observed in a comet. The finding adds to the evidence that comets could have been a source of the complex organic molecules necessary for the emergence of life.

"We found that comet Lovejoy was releasing as much alcohol as in at least 500 bottles of wine every second during its peak activity," said Nicolas Biver of the Paris Observatory, France, lead author of a paper on the discovery published Oct. 23 in Science Advances. The team found 21 organic molecules in gas from the comet, including ethyl alcohol and glycolaldehyde, a simple sugar.

Comets are frozen remnants from the formation of our solar system. Scientists are interested in them because they are relatively pristine and therefore hold clues to how the solar system was made. Most orbit in frigid zones far from the sun. However, occasionally, a gravitational disturbance sends a comet closer to the sun, where it heats up and releases gases, allowing scientists to determine its composition.

Comet Lovejoy was one of the brightest and most active comets since comet Hale-Bopp in 1997. Lovejoy passed closest to the sun on January 30, 2015, when it was releasing water at the rate of 20 tons per second. The team observed the atmosphere of the comet around this time when it was brightest and most active. They observed a microwave glow from the comet using the 30-meter (almost 100-foot) diameter radio telescope at Pico Veleta in the Sierra Nevada Mountains of Spain.

Sunlight energizes molecules in the comet's atmosphere causing them to glow at specific microwave frequencies (if microwaves were visible, different frequencies would be perceived as different colors). Each kind of molecule glows at specific, signature frequencies, allowing the team to identify it with detectors on the telescope. The advanced equipment was capable of analyzing a wide range of frequencies simultaneously, allowing the team to determine the types and amounts despite a short observation period.



"The result definitely promotes the idea the comets carry very complex chemistry," said Stefanie Milam of NASA's Goddard Space Flight Center in Greenbelt, Maryland, a co-author on the paper. "During the Late Heavy Bombardment about 3.8 billion years ago, when many comets and asteroids were blasting into Earth and we were getting our first oceans, life didn't have to start with just simple molecules like water, carbon monoxide, and nitrogen. Instead, life had something that was much more sophisticated on a molecular level. We're finding molecules with multiple carbon atoms. So now you can see where sugars start forming, as well as more complex organics such as amino acids—the building blocks of proteins—or nucleobases, the building blocks of DNA. These can start forming much easier than beginning with molecules with only two or three atoms."

## Further Information

In July, the European Space Agency reported that the Philae lander from its Rosetta spacecraft in orbit around comet 67P/Churyumov­-Gerasimenko detected 16 organic compounds as it descended toward and then bounced across the comet's surface. According to the agency, some of the compounds detected play key roles in the creation of amino acids, nucleobases, and sugars from simpler "building-block" molecules.

Astronomers think comets preserve material from the ancient cloud of gas and dust that formed the solar system. Exploding stars (supernovae) and the winds from red giant stars near the end of their lives produce vast clouds of gas and dust. Solar systems are born when shock waves from stellar winds and other nearby supernovae compress and concentrate a cloud of ejected stellar material until dense clumps of that cloud begin to collapse under their own gravity, forming a new generation of stars and planets.


These clouds contain countless dust grains. Carbon dioxide, water, and other gases form a layer of frost on the surface of these grains, just as frost forms on car windows during cold, humid nights. Radiation in space powers chemical reactions in this frost layer to produce complex organic molecules. The icy grains become incorporated into comets and asteroids, some of which impact young planets like ancient Earth, delivering the organic molecules contained within them.

"The next step is to see if the organic material being found in comets came from the primordial cloud that formed the solar system or if it was created later on, inside the protoplanetary disk that surrounded the young sun," said Dominique Bockelée-Morvan from Paris Observatory, a co-author of the paper.


Learn More!



# Hi Erins